﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_Analysis_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double a, b, c;

        private void bPlus_Click(object sender, EventArgs e)
        {
            lblOperation.Text = "+";
            tbResult.Text = (a + b).ToString();
        }

        private void bMinus_Click(object sender, EventArgs e)
        {
            lblOperation.Text = "-";
            tbResult.Text = (a - b).ToString();
        }

        private void bMulti_Click(object sender, EventArgs e)
        {
            lblOperation.Text = "*";
            tbResult.Text = (a * b).ToString();
        }

        private void bSine_Click(object sender, EventArgs e)
        {
            tbCResult.Text = Math.Sin(c).ToString();
        }

        private void bCosine_Click(object sender, EventArgs e)
        {
            tbCResult.Text = Math.Cos(c).ToString();
        }

        private void bRoot_Click(object sender, EventArgs e)
        {
            tbCResult.Text = Math.Sqrt(c).ToString();
        }

        private void bSquare_Click(object sender, EventArgs e)
        {
            tbCResult.Text = (c*c).ToString();
        }

        private void bLog_Click(object sender, EventArgs e)
        {
            tbCResult.Text = Math.Log10(c).ToString();
        }

        private void bLn_Click(object sender, EventArgs e)
        {
            tbCResult.Text = Math.Log(c).ToString();
        }

        private void bCClear_Click(object sender, EventArgs e)
        {
            tbOperator.Text = "";
            tbCResult.Text = "";
        }

        private void bAllClear_Click(object sender, EventArgs e)
        {
            lblOperation.Text = "";
            tbOperator1.Text = "";
            tbOperator2.Text = "";
            tbResult.Text = "";
            tbOperator.Text = "";
            tbCResult.Text = "";
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tbOperator1_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(tbOperator1.Text, out a);
        }

        private void tbOperator2_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(tbOperator2.Text, out b);
        }

        private void tbOperator_TextChanged(object sender, EventArgs e)
        {
            double.TryParse(tbOperator.Text, out c);
        }

        private void bDivide_Click(object sender, EventArgs e)
        {
            lblOperation.Text = "/";
            tbResult.Text = (a / b).ToString();
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            lblOperation.Text = "";
            tbOperator1.Text = "";
            tbOperator2.Text = "";
            tbResult.Text = "";
        }
    }
}
