﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsFormsApp1.Hanged
{
    public class HngedMan
    {
        private List<string> wordList;
        private char letter;
        private string word;

        public HngedMan()
        {
            wordList = new List<string>();
            using (System.IO.StreamReader read = new System.IO.StreamReader("list.txt"))
            {
                while(!read.EndOfStream)
                {
                    String row = read.ReadLine();
                    wordList.Add(row);
                }
                RandomWord();
            }
        }

        private void RandomWord()
        {
            Random rand = new Random();
            word = wordList[rand.Next(0, wordList.Count)];
            word = word.ToUpper();
        }

        public char GetLetter()
        {
            return this.letter;
        }

        public void SetLetter(char letter)
        {
            this.letter = letter;
        }

        public List<int> FindLetter()
        {
            char[] letterN = word.ToCharArray();
            List<int> listOfIndexes = new List<int>();
            for(int i=0;i<word.Length;i++)
            {
                if (letter == letterN[i])
                    listOfIndexes.Add(i);
            }

            return listOfIndexes;
        }

        public string GetWord()
        {
            return this.word;
        }

        public int GetWordLength()
        {
            return word.Length;
        }

       
        
    }
}
