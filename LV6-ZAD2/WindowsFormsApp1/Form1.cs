﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Hanged;

namespace WindowsFormsApp1
{
    public partial class tTries : Form
    {
        public tTries()
        {
            InitializeComponent();
        }

        private List<Label> hmWord = new List<Label>();
        private HngedMan h;
        private int right = 0;

        private void bExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bNewGame_Click(object sender, EventArgs e)
        {
            this.h = new HngedMan();
            #region Button enabling
            bA.Enabled = true;
            bB.Enabled = true;
            bC.Enabled = true;
            bD.Enabled = true;
            bE.Enabled = true;
            bF.Enabled = true;
            bG.Enabled = true;
            bH.Enabled = true;
            bI.Enabled = true;
            bJ.Enabled = true;
            bK.Enabled = true;
            bL.Enabled = true;
            bM.Enabled = true;
            bN.Enabled = true;
            bO.Enabled = true;
            bP.Enabled = true;
            bQ.Enabled = true;
            bR.Enabled = true;
            bS.Enabled = true;
            bT.Enabled = true;
            bU.Enabled = true;
            bV.Enabled = true;
            bW.Enabled = true;
            bX.Enabled = true;
            bY.Enabled = true;
            bZ.Enabled = true;
            #endregion

            foreach (Label label in this.hmWord) this.Controls.Remove(label);
            if (this.hmWord.Count > 0) this.hmWord.Clear();

            int startX = 13;
            String word = this.h.GetWord();
            Console.WriteLine(word);
            foreach (char c in word)
            {
                Label lbl = new Label();
                lbl.Text = "_";
                lbl.Font = new Font(this.Font.Name, 35, FontStyle.Underline);
                lbl.Location = new Point(startX, 9);
                lbl.Tag = c.ToString();
                lbl.AutoSize = true;
                startX = lbl.Right;
                this.Controls.Add(lbl);
                this.hmWord.Add(lbl);
            }

            bNewGame.Enabled = false;
            bReset.Enabled = true;

        }

        private void handleGuess(char letter, Button btn)
        {
            h.SetLetter(letter);
            Console.WriteLine(h.FindLetter());
            List<int> listOfIndexes = h.FindLetter();
            if (listOfIndexes.Count() > 0)
            {
                btn.Enabled = false;
                Console.WriteLine(h.FindLetter());
                listOfIndexes.ForEach(delegate (int index) { hmWord[index].Text = letter.ToString(); right += 1; });

            }
            else
            {
                int numb = int.Parse(tbNoOTries.Text);
                numb -= 1;
                tbNoOTries.Text = numb.ToString();
                btn.Enabled = false;
                if (numb == 0)
                {
                    MessageBox.Show("Game over", "Game over");
                    Application.Restart();
                }

               
            }

            if(this.h.GetWordLength() == right)
            {
                MessageBox.Show("You've won", "Winner");
                Application.Restart();
            }
        }

        private char getBtnChar(Button btn)
        {
            return btn.Text[0];
        }

        private void bA_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);

        }

        private void bB_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bZ_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bY_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bX_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bL_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bK_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bJ_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bI_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bW_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bV_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bU_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bT_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bH_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bG_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bF_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bE_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bS_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bR_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bQ_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bD_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bC_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bM_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bP_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bO_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bN_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            this.handleGuess(this.getBtnChar(btn), btn);
        }

        private void bReset_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

    }
}
